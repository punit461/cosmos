import React, { useCallback, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import { editUser } from "../Service/users";
import BottomPopup from "./components/BottomPopup";

const UserDetails = ({ route }) => {
  const { user } = route.params;
  const [showConfirm, setShowConfirm] = useState(false);
  const [userData, setUserData] = useState(user);
  const navigation = useNavigation();

  const handleChange = useCallback((field, text) => {
    setUserData((u) => {
      const changeUser = {
        ...u,
        [field]: text,
      };
      return changeUser;
    });
  }, []);

  const handleSave = () => {
    setShowConfirm(false);
    editUser(userData)
      .then(() => {
        Toast.show({
          type: "success",
          text1: "Data saved successfully.",
        });
        setTimeout(() => {
          navigation.goBack();
        }, 1000);
      })
      .catch(() => {
        Toast.show({
          type: "error",
          text1: "Something went wrong.",
        });
      });
  };

  const handleClear = useCallback(() => {
    setUserData(user);
  }, [user]);

  return (
    <ScrollView>
      <View style={styles.detailsContainer}>
        <Image
          id="imgProfile"
          style={styles.detailsImage}
          source={{ uri: user.profilepic }}
        />
        <Text
          id="lblName"
          style={styles.detailsName}
        >{`${user.firstname} ${user.lastname}`}</Text>
        <Text id="lblEmail" style={styles.detailsEmail}>
          {user.email}
        </Text>

        <Text style={styles.labelEdit}>Edit Details : </Text>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Email:</Text>
          <TextInput
            id="txtEmail"
            style={styles.textInput}
            placeholder="Enter your email"
            value={userData.email}
            onChangeText={(text) => handleChange("email", text)}
            textAlign="left"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>First Name:</Text>
          <TextInput
            id="txtFirstName"
            style={styles.textInput}
            placeholder="Enter your first name"
            value={userData.firstname}
            onChangeText={(text) => handleChange("firstname", text)}
            textAlign="left"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Last Name:</Text>
          <TextInput
            id="txtLastName"
            style={styles.textInput}
            placeholder="Enter your last name"
            value={userData.lastname}
            onChangeText={(text) => handleChange("lastname", text)}
            textAlign="left"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Phone:</Text>
          <TextInput
            id="txtPhone"
            style={styles.textInput}
            placeholder="Enter your phone"
            value={userData.phone}
            onChangeText={(text) => handleChange("phone", text)}
            textAlign="left"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>City:</Text>
          <TextInput
            id="txtCity"
            style={styles.textInput}
            placeholder="Enter your city"
            value={userData.city}
            onChangeText={(text) => handleChange("city", text)}
            textAlign="left"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>State:</Text>
          <TextInput
            id="txtState"
            style={styles.textInput}
            placeholder="Enter your state"
            value={userData.state}
            onChangeText={(text) => handleChange("state", text)}
            textAlign="left"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Country:</Text>
          <TextInput
            id="txtCountry"
            style={styles.textInput}
            placeholder="Enter your country"
            value={userData.country}
            onChangeText={(text) => handleChange("country", text)}
            textAlign="left"
          />
        </View>

        <BottomPopup
          visible={showConfirm}
          handleConfirm={handleSave}
          outsideClick={() => setShowConfirm(false)}
        />

        <View style={styles.buttonContainer}>
          <TouchableOpacity
            id="btnReset"
            style={[styles.loginButton, styles.buttonSecondary]}
            onPress={handleClear}
          >
            <Text style={styles.buttonText}>Reset</Text>
          </TouchableOpacity>
          <TouchableOpacity
            id="btnSave"
            style={[styles.loginButton, styles.buttonPrimary]}
            onPress={() => setShowConfirm(true)}
          >
            <Text style={styles.buttonText}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  detailsContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
    paddingVertical: 20,
  },
  detailsImage: {
    width: 150,
    height: 150,
    borderRadius: 75,
    marginBottom: 20,
  },
  detailsName: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  detailsEmail: {
    fontSize: 16,
    color: "#666",
    marginBottom: 50,
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
    marginRight: 10,
  },
  labelEdit: {
    fontWeight: "bold",
    fontSize: 16,
    alignSelf: "flex-start",
    marginLeft: 10,
    marginBottom: 20,
  },
  label: {
    marginRight: 10,
    marginHorizontal: 10,
    flex: 1,
  },
  textInput: {
    flex: 3,
    height: 40,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    paddingLeft: 10,
  },
  loginButton: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 8,
    marginVertical: 10,
    marginHorizontal: 10,
  },
  buttonPrimary: {
    backgroundColor: "#3498db",
  },
  buttonSecondary: {
    backgroundColor: "#aeaeae",
  },
  buttonText: {
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
  buttonContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
});

export default UserDetails;
