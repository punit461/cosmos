import React, { useCallback, useEffect, useRef, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome6";
import { useNavigation } from "@react-navigation/native";
import Spinner from "react-native-loading-spinner-overlay";
import Toast from "react-native-toast-message";
import { saveUsers } from "../Service/db";
import { getUsers } from "../Service/users";
import { sanitizeData } from "../Utils/users";
import NoData from "./components/NoData";
import { useAuth } from "./AuthenticationProvider";

const screenWidth = Dimensions.get("window").width;
const USERS_LIST_URL = "https://randomuser.me/api/?results=150";

const User = ({ data, handleClick }) => {
  return (
    <TouchableOpacity
      id={`listUser${data.id}`}
      onPress={() => {
        handleClick(data);
      }}
    >
      <View id={`cardUser${data.id}`} style={styles.userContainer}>
        <Image
          id={`imgProfile${data.id}`}
          style={styles.userImage}
          source={{ uri: data.thumbnail }}
        />
        <View style={styles.userInfo}>
          <Text
            id={`lblName${data.id}`}
            style={styles.userName}
          >{`${data.firstname} ${data.lastname}`}</Text>
          <Text id={`lblEmail${data.id}`} style={styles.userEmail}>
            {data.email}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const Users = () => {
  const {isLoggedIn} = useAuth();
  const flatListRef = useRef(null);
  const [isLoading, setIsLoading] = useState(true);
  const [users, setUsers] = useState([]);
  const [filteredMaleUsers, setFilteredMaleUsers] = useState([]);
  const [filteredFemaleUsers, setFilteredFemaleUsers] = useState([]);
  const [searchText, setSearchText] = useState("");
  const navigation = useNavigation();

  const loadData = useCallback(async () => {
    try {
      const storedUsers = await getUsers();
      if (storedUsers.length) {
        setUsers(storedUsers);
        setFilteredMaleUsers(
          storedUsers.filter((user) => user.gender === "male")
        );
        setFilteredFemaleUsers(
          storedUsers.filter((user) => user.gender === "female")
        );
      } else {
        const randomUsersApiResponse = await fetch(USERS_LIST_URL);
        const { results } = await randomUsersApiResponse.json();
        const userList = sanitizeData(results);
        setUsers(userList);
        setFilteredMaleUsers(userList.filter((user) => user.gender === "male"));
        setFilteredFemaleUsers(
          userList.filter((user) => user.gender === "female")
        );
        await saveUsers(userList);
      }
    } catch (error) {
      Toast.show({
        type: "error",
        text1: "Something went wrong. Error loading data.",
      });
    }
    setIsLoading(false);
  }, []);

  useEffect(() => {
    if (!isLoggedIn) {
      navigation.navigate("Login")
    }
  }, [navigation, isLoggedIn]);

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => null,
    });
    loadData();
  }, [navigation, loadData]);

  useEffect(() => {
    const focusListener = navigation.addListener("focus", loadData);
    return () => {
      focusListener();
    };
  }, [loadData, navigation]);

  const handleSearch = (userName) => {
    setSearchText(userName);
    const filteredMale = users.filter(
      (user) =>
        user.gender === "male" &&
        `${user.firstname.toLowerCase()} ${user.lastname.toLowerCase()}`.includes(
          userName.toLowerCase()
        )
    );
    const filteredFemale = users.filter(
      (user) =>
        user.gender === "female" &&
        `${user.firstname.toLowerCase()} ${user.lastname.toLowerCase()}`.includes(
          userName.toLowerCase()
        )
    );
    setFilteredMaleUsers(filteredMale);
    setFilteredFemaleUsers(filteredFemale);
  };

  const navigateToUserDetails = (user) => {
    navigation.navigate("UserDetails", { user });
  };

  const handleScrollToIndex = (isMale, index) => {
    const dataToScroll = isMale ? filteredMaleUsers : filteredFemaleUsers;
    if (dataToScroll?.length) {
      flatListRef.current.scrollToIndex({
        animated: true,
        index: dataToScroll.findIndex((item, i) => i === index),
      });
    }
  };

  return (
    <View style={styles.searchContainer}>
      {isLoading && <Spinner
        id="loading"
        visible={isLoading}
        textContent={""}
        overlayColor="rgba(0, 0, 0, 0.40)"
      />}
      <View style={styles.searchHeader}>
        <Icon name="search" size={24} style={styles.searchIcon} />
        <TextInput
          id="txtSearch"
          style={styles.searchInput}
          placeholder="Search users..."
          value={searchText}
          onChangeText={handleSearch}
        />
      </View>
      <FlatList
        ref={flatListRef}
        data={["male", "female"]}
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item) => item}
        renderItem={({ item }) => (
          <FlatList
            data={item === "male" ? filteredMaleUsers : filteredFemaleUsers}
            renderItem={({ item }) => (
              <User data={item} handleClick={navigateToUserDetails} />
            )}
            keyExtractor={(user) => user.email}
            ListEmptyComponent={NoData}
          />
        )}
      />
      <View style={styles.tabsContainer}>
        {[
          { id: 0, value: "Male" },
          { id: 1, value: "Female" },
        ].map((i) => (
          <TouchableOpacity
            key={i.value}
            style={styles.tab}
            onPress={() => {
              handleSearch("");
              setSearchText("");
              handleScrollToIndex(true, i.id);
            }}
          >
            <Text id={`tab${i.value}`} style={styles.tabText}>
              {i.value}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    flex: 1,
    backgroundColor: "#fff",
  },
  searchHeader: {
    flexDirection: "row",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  searchInput: {
    flex: 2,
    height: 40,
    backgroundColor: "#f2f2f2",
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  userContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginHorizontal: 16,
    marginTop: 8,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 8,
    width: screenWidth - 26,
  },
  userImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 10,
  },
  userInfo: {
    flex: 1,
  },
  searchIcon: {
    marginRight: 10,
    marginTop: 5,
  },
  userName: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 5,
  },
  userEmail: {
    color: "#666",
  },
  tabsContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  tab: {
    alignItems: "center",
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  tabText: {
    fontSize: 16,
    fontWeight: "bold",
  },
  activeTab: {
    borderBottomWidth: 2,
    borderBottomColor: "#000",
  },
});

export default Users;
