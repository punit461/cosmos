import React from "react";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import {
  View,
  Text,
  TouchableOpacity,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
} from "react-native";

const BottomPopup = ({
  visible = false,
  mode = "overFullScreen",
  outsideClick = () => {},
  dismiss = () => {},
  handleConfirm,
}) => {
  const { bottom } = useSafeAreaInsets();
  return (
    <Modal
      animationType="slide"
      transparent
      visible={visible}
      onDismiss={dismiss}
      onRequestClose={outsideClick} // for android back button
      presentationStyle={mode}
    >
      <TouchableWithoutFeedback onPress={outsideClick}>
        <View style={styles.popUpWrapper}>
          <View style={styles.childContainer("#FFFFFF")}>
            <View style={styles.popupContainer(bottom)}>
              <Text id="txtPopupConfirm" style={styles.text}>
                Are you sure, you want to save the changes ?
              </Text>
              <TouchableOpacity
                id="btnPopupConfirm"
                onPress={handleConfirm}
                style={styles.button}
              >
                <Text style={styles.buttonText}>Confirm</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const styles = StyleSheet.create({
  popUpWrapper: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-end",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  childContainer: (backgroundColor) => ({
    backgroundColor,
    flex: 1,
    alignItems: "center",
    paddingBottom: 30,
  }),
  popupContainer: (bottom) => ({
    paddingBottom: bottom < 16 ? bottom : 16,
    paddingHorizontal: 16,
    paddingTop: 32,
  }),
  buttonText: {
    color: "#FFFFFF",
  },
  text: {
    textAlign: "center",
    marginBottom: 50,
    fontSize: 24,
  },
  button: {
    height: 48,
    borderRadius: 8,
    backgroundColor: "#3498db",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default BottomPopup;
