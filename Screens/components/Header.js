import React from "react";
import {
  Dimensions,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
} from "react-native";
import { useAuth } from "../AuthenticationProvider";

const screenWidth = Dimensions.get("window").width;

const Header = ({ title }) => {
  const { logout } = useAuth();

  const handleLogout = () => {
    logout();
  };

  return (
    <View style={styles.container}>
      <Text id="txtWelcomeUser">Hi, admin</Text>
      <Text style={styles.title} id="txtTitle">
        {title}
      </Text>
      <TouchableOpacity id="btnLogout" onPress={handleLogout}>
        <Text style={styles.button}>Logout</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: screenWidth - 50,
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 15,
  },
  title: { fontWeight: "500" },
  button: { color: "#3498db" },
});

export default Header;
