import React from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";

const screenWidth = Dimensions.get("window").width;

const NoData = () => {
  return (
    <View style={styles.userContainer}>
      <Text style={styles.nodata} id="txtNoDataMessage">
        No data available
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  userContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginHorizontal: 16,
    marginTop: 8,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 8,
    width: screenWidth - 26,
  },
  nodata: {
    textAlign: "center",
  },
});

export default NoData;
