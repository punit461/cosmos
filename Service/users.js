import { getDBConnection } from "./db";

const tableName = "employees";
export const getUsers = async () => {
  try {
    const db = await getDBConnection();
    const users = [];
    const results = await db.executeSql(`SELECT * FROM ${tableName}`);
    results.forEach((result) => {
      for (let index = 0; index < result.rows.length; index++) {
        users.push(result.rows.item(index));
      }
    });
    return users;
  } catch (error) {
    console.error(error);
    throw Error("Failed to get users !!!");
  }
};

export const editUser = async (user) => {
  const db = await getDBConnection();
  const insertQuery =
    `INSERT OR REPLACE INTO ${tableName}(id, firstname, lastname, gender, profilepic, email, phone, city, state, country, thumbnail) values` +
    `(${user.id}, '${user.firstname}', '${user.lastname}', '${user.gender}', '${user.profilepic}', '${user.email}', '${user.phone}', '${user.city}', '${user.state}', '${user.country}', '${user.thumbnail}')`;

  return db.executeSql(insertQuery);
};

export const deleteUser = async (id) => {
  const db = await getDBConnection();
  const deleteQuery = `DELETE from ${tableName} where id = ${id}`;
  await db.executeSql(deleteQuery);
};

export const deleteTable = async () => {
  const db = await getDBConnection();
  const query = `DROP TABLE IF EXISTS ${tableName}`;

  await db.executeSql(query);
};
