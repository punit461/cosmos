import { enablePromise, openDatabase } from "react-native-sqlite-storage";

const tableName = "employees";

enablePromise(true);

export const getDBConnection = async () => {
  return openDatabase({ name: "rocked.db", location: "default" });
};

export const createTable = async () => {
  const db = await getDBConnection();
  // create table if not exists
  const query = `CREATE TABLE IF NOT EXISTS ${tableName}(
        id INTEGER PRIMARY KEY,
        firstname TEXT NOT NULL,
	    lastname TEXT NOT NULL,
	    email TEXT NOT NULL UNIQUE,
	    phone TEXT NOT NULL UNIQUE,
        gender TEXT NOT NULL,
        profilepic TEXT NOT NULL,
        city TEXT NOT NULL,
        state TEXT NOT NULL,
        country TEXT NOT NULL,
        thumbnail TEXT NOT NULL
    ) WITHOUT ROWID;`;

  await db.executeSql(query);
};

export const saveUsers = async (users) => {
  const db = await getDBConnection();
  const insertQuery =
    `INSERT OR REPLACE INTO ${tableName}(id, firstname, lastname, gender, profilepic, email, phone, city, state, country, thumbnail) values` +
    users
      .map(
        (i) =>
          `(${i.id}, '${i.firstname}', '${i.lastname}', '${i.gender}', '${i.profilepic}', '${i.email}', '${i.phone}', '${i.city}', '${i.state}', '${i.country}', '${i.thumbnail}')`
      )
      .join(",");

  return db.executeSql(insertQuery);
};
