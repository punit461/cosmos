export const sanitizeData = (data) =>
  data.map((val, index) => ({
    id: index,
    firstname: val.name.first,
    lastname: val.name.last,
    gender: val.gender,
    thumbnail: val.picture.thumbnail,
    profilepic: val.picture.large,
    email: val.email,
    phone: val.phone,
    city: val.location.city,
    state: val.location.state,
    country: val.location.country,
  }));
